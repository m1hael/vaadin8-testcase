package vaadin8.showcase;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
public class Application extends UI {

    private static final long serialVersionUID = 7692586665156023329L;

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = Application.class)
    public static class Servlet extends VaadinServlet {
        private static final long serialVersionUID = -7645224332694970915L;
    }

    @Override
    protected void init(VaadinRequest request) {
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setWidth("100%");
        mainLayout.setHeight(null);
        mainLayout.setMargin(true);

        Button login = new Button("login");
        login.addClickListener(new ClickListener() {

            private static final long serialVersionUID = -2623705870119384633L;

            @Override
            public void buttonClick(ClickEvent event) {
                addWindow(new LoginWindow());
            }
        });
        mainLayout.addComponent(login);

        Component form = createPositionForm();
        mainLayout.addComponent(form);

        setContent(mainLayout);
    }

    private Component createPositionForm() {
        FormLayout form = new FormLayout();

        Position position = new Position(); // (1, "Caretaker");
        Binder<Position> binder = new Binder<Position>(Position.class);
        binder.setBean(position);

        TextField idField = new TextField("Id:");
        binder.
            forField(idField).
            asRequired("Please enter an id for this position.").
            withConverter(new StringToIntegerConverter("Only numbers are valid entries for the id.")).
            bind("id");
        form.addComponent(idField);

        TextField nameField = new TextField("Name:");
        nameField.setRequiredIndicatorVisible(true);
        binder.
            forField(nameField).
            asRequired("Please enter a name for this position.").
            bind("name");
        form.addComponent(nameField);

        return form;
    }

}
