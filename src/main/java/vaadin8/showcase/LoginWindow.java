package vaadin8.showcase;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class LoginWindow extends Window {

    private static final long serialVersionUID = -941940293324326877L;

    private TextField usernameField;
    private PasswordField passwordField;

    private FormLayout layout;
    
    public LoginWindow() {
        super("Login");
        setWidth("450px");
        setHeight("300px");
        setModal(true);
        
        addStyleName("login");
        
        initComponents();
    }
    
    private void addCustomStyles() {
        Page.getCurrent().getStyles().add(
                        ".login {"
                        + "background-color: #243E84 !important;"
                        + "} "
                        + ".login .v-window-header {"
                        + "color: white; "
                        + "font-weight: bold; "
                        + "} "
                        + ".login .v-window-maximizebox {"
                        + "background-color: #243E84 !important;"
                        + "color: white; "
                        + "} "
                        + ".login .v-window-closebox {"
                        + "background-color: #243E84 !important;"
                        + "color: white; "
                        + "} "
                        + ".login .v-caption span {"
                        + "color: white !important; "
                        + "} "
                        + ".login .v-label { "
                        + "color: white !important; "
                        + "}" 
                        );
    }

    private void initComponents() {
        addCustomStyles();
        
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.setSpacing(false);
        
        mainLayout.addComponent(new Label("Benutzernamen und Passwort eingeben:"));
        
        layout = new FormLayout();
        usernameField = new TextField("Benutzername:");
        usernameField.setWidth("100%");
        layout.addComponent(usernameField);
        passwordField = new PasswordField("Passwort:");
        passwordField.setWidth("100%");
        layout.addComponent(passwordField);
        layout.addComponent(createButtons());
        
        mainLayout.addComponent(layout);
        
        setContent(mainLayout);
    }

    @Override
    public void attach() {
        super.attach();
        focus();
    }
    
    @Override
    public void focus() {
        super.focus();
        usernameField.focus();
    }
    
    private Component createButtons() {
        HorizontalLayout layout = new HorizontalLayout();
        
        Button login = new Button("Login");
        login.setClickShortcut(KeyCode.ENTER, null);
        login.addStyleName("primary");
        login.addClickListener(new ClickListener() {
            
            private static final long serialVersionUID = -8404122875974261481L;

            @Override
            public void buttonClick(ClickEvent event) {
                try {
                    String creds = usernameField.getValue() + ":" + passwordField.getValue();
                    Notification.show(creds);
                    
                }
                catch (Exception e) {
                    Notification.show(e.getMessage());
                }
            }
        });
        layout.addComponent(login);
        
        return layout;
    }
}
